class ChangeIngredientType < ActiveRecord::Migration[5.0]
  def change
    change_column :recipes, :ingredients, :json, default: '{}'
  end
end
