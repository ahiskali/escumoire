class AddTagCount < ActiveRecord::Migration[5.0]
  def change
    change_table :tags do |t|
      t.integer :count, default: 0
    end
    add_index :tags, :count, order: {count: :desc}
  end
end
