# Esqumoire

Esqumoire is a recipe book for all kinds of crazy recipes.

It's live at [https://escumoire.herokuapp.com/](https://escumoire.herokuapp.com/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
