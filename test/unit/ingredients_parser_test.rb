require 'test_helper'
require_relative "../../lib/ingredients_parser.rb"

class IngredientsParserTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "can parse ingredients statrting with the ingredient name" do
    i = IngredientsParser.new
    input = "крысиный хвост 2 штуки"
    assert_equal([{ name: "крысиный хвост", value: 2, unit: "штуки", string: input }], i.parse(input))
  end
  test "can parse ingredients ending with the ingredient name" do
    i = IngredientsParser.new
    input = "300 грамм звездной пыли"
    assert_equal([{ name: "звездной пыли", value: 300, unit: "грамм", string: input}], i.parse(input))
  end
  test "can parse ingredients without unit" do
    i = IngredientsParser.new
    input = "300 нейтронов"
    assert_equal([{ name: "нейтронов", value: 300, unit: nil, string: input }], i.parse(input))
    input = "глазные яблоки 20"
    assert_equal([{ name: "глазные яблоки", value: 20, unit: nil, string: input }], i.parse(input))
  end
  test "can parse ingredients without quantity" do
    i = IngredientsParser.new
    input = "яичная скорлупа Оккамия"
    assert_equal([{ name: "яичная скорлупа Оккамия", value: nil, unit: nil, string: input }], i.parse(input))
  end
  test "can parse multiple strings" do
    i = IngredientsParser.new
    input = """\
    1 яйцо огневицы (пеплозмея)
    сок луковицы морского лука 200 миллилитров
    наросты со спины Растопырника\
    """
    parsed_ingredients = [
      { name: "яицо огневицы (пеплозмея)", value: 1, unit: nil, string: "300 грамм звездной пыли"},
      { name: "сок луковицы морского лука", value: 200, unit: "миллилитров", string: "сок луковицы морского лука 200 миллилитров"},
      { name: "наросты со спины Растопырника", value: nil, unit: nil, string: "наросты со спины Растопырника"}
    ]
    assert_equal(parsed_ingredients, i.parse(input))
  end
end
