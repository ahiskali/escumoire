class TagsController < ApplicationController

  def index
    @tags = Tag.all
    @sum = @tags.inject(0) { |sum, tag| sum + tag.count}
  end

  def show
    if tag
      @recipes = tag.recipes.page(params[:page]).per(5)
    else
      @recipes = Recipe.none.page(params[:page]).per(5)
    end
    render 'recipes/index'
  end

  private

  def tag
    if params[:name].present?
      Tag.find_by_name(params[:name])
    end
  end

end
