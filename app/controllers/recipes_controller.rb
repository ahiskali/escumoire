class RecipesController < ApplicationController

  def index
    if params[:query].present?
      @recipes = Recipe.search(params[:query]).page(params[:page]).per(5)
    else
      @recipes = Recipe.all.page(params[:page]).per(5)
    end
  end

  def new
    @recipe = Recipe.new
    @popular_tags = Tag.order(count: :desc).first(5)
  end

  def create
    @recipe = Recipe.new(recipe_params)
    if @recipe.save
      flash[:success] = "Recipe saved"
      redirect_to recipes_path
    else
      flash[:danger] =  @recipe.errors.full_messages.join(", ")
      render :new
    end
  end

  private
    def recipe_params
       raw_params = params.require(:recipe).permit(:name, :instructions, :ingredients_list, :tag_names)
       raw_params
    end
end
