class Recipe < ApplicationRecord
  include PgSearch
  has_many :taggings
  has_many :tags, through: :taggings
  validates :name, presence: true
  validates :instructions, presence: true
  pg_search_scope :search, against: [:name, :instructions]
  serialize :ingredients, JSON
  paginates_per 50

  def ingredients_list
    ingredients.map { |i| i["string"] } if ingredients.any?
  end

  def ingredients_list=(input)
    self.ingredients = IngredientsParser.parse(input)
  end

  def tag_names
    tags.map(&:name).join(", ")
  end

  def tag_names=(names)
    self.tags = names.split(",").map do |name|
      t = Tag.where(name: name.strip).first_or_create!
      t.count += 1
      t
    end
  end

end
