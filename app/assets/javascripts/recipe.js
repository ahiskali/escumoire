var tagsinput = function() {
  $('#tagsinput').tagsInput({
    'height':'100%',
    'width':'100%',
    'min-height':'auto',
    'defaultText':'',
    'border':'auto',
    'placeholderColor' : '#B5AC49',
  });
  $("#tagsinput_tagsinput").addClass('form-control');
};

var populartags = function() {
  $(".populartag").click( function() {
    $('#tagsinput').addTag($(this).text());
    $(this).remove();
    }
  );
}
var new_recipe_page = function () {
  tagsinput();
  populartags();
}
