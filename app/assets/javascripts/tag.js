var tags_page = function () {
  $.fn.tagcloud.defaults = {
      size: {start: 10, end: 20, unit: 'pt'},
      color: {start: '#B5AC49', end: '#3CA55C'}
    };

  $(function () {
    $('#tagcloud a').tagcloud();
  });
}
