Rails.application.routes.draw do
  root to: "pages#index"
  get 'about', to: 'pages#about'
  resources :recipes, only: [:index, :new, :create]
  resources :tags, only: [:index, :show], param: :name
end
