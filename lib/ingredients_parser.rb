class IngredientsParser

  UNITS = "на кончике ножа|столов(?:ая|ые|ых) лож|чайн(?:ая|ые|ых) лож|миллилитр\
  |килограмм|по вкусу|стакан|зубчик|веточ|щепот|грамм|голов|стеб|штук|литр|дэш\
  |пуч|кус|бaн|кг|мл|г|л"
  SUFFIXES = "лей|ель|ка|ки|ок|ов|ек|ля|ей|a|и"

  def self.parse(input)
    return if !input
    ingredients = []
    input.each_line do |line|
      quantity = line.match /(\d+) ?((?:#{UNITS})?(?:#{SUFFIXES})?)/i
      name = quantity ? [quantity.pre_match, quantity.post_match].max : line
      ingredient = {
        name: name.strip,
        value: (quantity[1].to_i if quantity),
        unit: (quantity[2].strip if quantity && !quantity[2].strip.empty?),
        string: line.downcase
      }
      ingredients << ingredient
    end
    ingredients
  end

end
